# -*- coding: utf-8 -*-
# @Time    : 2019/3/12 11:46
# @Author  : zzt

import allure
import pytest

@allure.feature('这里是一级标签')
class TestAllure():

    @allure.title("用例标题0")
    @allure.description("这里是对test_0用例的一些详细说明")
    @allure.story("这里是第一个二级标签")
    def test_0(self):
        pass

    @allure.title("用例标题1")
    @allure.story("这里是第二个二级标签")
    def test_1(self):
        pass

    @allure.title("用例标题2")
    @allure.story("这里是第三个二级标签")
    def test_2(self):
        pass
if __name__ == '__main__':
    pytest.main(['-s', '-q', '--alluredir', '../allure-report/xml'])