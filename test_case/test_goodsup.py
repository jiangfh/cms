import pytest
from time import sleep
import allure
#from element.page_element import *
#from baseclass.basepage import BasePage
from pages.buildnewgoods import BuildGoods
from testdata.data import add_goods_canshu
@allure.feature("商品管理")
class TestIndexPage():
    #@pytest.mark.repeat(1)
    @allure.title("新建商品")
    @pytest.mark.parametrize("data",add_goods_canshu)
    def test_add_goods(self,data,init_web):
        self.driver,self.login_page=init_web
        #调用时间戳的方法
        #@allure.step("获取时间")
        OtherStyleTime=BuildGoods(self.driver).now_time()
        #添加商品
        #@allure.step("新建商品")
        BuildGoods(self.driver).enter_build_goods(OtherStyleTime,data['goodsname1'],data['goodssimplename1'],data['maidian1'],data['chandi1'],data['shuliang1'])
        sleep(1)

if __name__ == '__main__':
    pytest.main(["-s","-q","--alluredir","./allure-report/xml"])
    #pytest.main(["-m", "login", "-s", "-q", "--alluredir", "./report"])