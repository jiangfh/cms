from selenium.webdriver.support.wait import WebDriverWait
import datetime
from selenium.webdriver.remote.webelement import WebElement
#from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.select import Select


class BasePage:

    def __init__(self,driver):
        self.driver=driver
    def wait_presence_element(self,loctor,timeout=10,poll_frequency=0.1):
        try:
            wait = WebDriverWait(self.driver, timeout, poll_frequency)
            find_element = wait.until(ec.presence_of_element_located(loctor))
            return find_element
        except Exception as e:
            print('元素定位失败',format(loctor))
    def wait_clickable_element(self,loctor,timeout=10,poll_frequency=0.1):
        try:
            wait = WebDriverWait(self.driver, timeout, poll_frequency)
            find_element = wait.until(ec.element_to_be_clickable(loctor))
            return find_element
        except Exception as e:
            print('元素定位错误',format(loctor))

    """select下拉框"""
    def select_new(self,canshu1,canshu2):
        select=self.wait_presence_element(canshu1)
        sel_new=Select(select)
        sel_new.select_by_visible_text(canshu2)

    def now_time(self):
        now=datetime.datetime.now()
        OtherStyleTime=now.strftime('%Y-%m-%d %H:%M:%S')
        return OtherStyleTime





