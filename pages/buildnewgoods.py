from baseclass.basepage import BasePage
from element.page_element import *
from element.parameter import *


class BuildGoods(BasePage):
    def enter_build_goods(self, OtherStyleTime, goodsname1, goodssimplename1, maidian1, chandi1,shuliang1):  # 传参数形式，方便后面ddt使用
        # 点击商品管理
        self.wait_presence_element(Home_GoodsManagement).click()
        # 点击商品上架
        self.wait_presence_element(Home_GoodsShelves).click()
        # 点击添加商品
        self.wait_presence_element(AddGoods).click()
        # 点击请选择寻味师
        self.wait_presence_element(AddGoods_Xunwei).click()
        # 选择寻味师
        self.wait_presence_element(AddGoods_XunWeiFirst).click()
        # 输入商品名称
        self.wait_presence_element(AddGoods_GoodsName).send_keys(OtherStyleTime + goodsname1)
        # 输入商品简称
        self.wait_presence_element(AddGoods_GoodsShortName).send_keys(OtherStyleTime+goodssimplename1)
        # 所属站点
        self.select_new(AddGoods_ZhanDian, "河北省")
        # 卖点介绍
        self.wait_presence_element(AddGoods_SellPoint).send_keys(maidian1)
        # 产地
        self.wait_presence_element(AddGoods_GoodArea).send_keys(chandi1)
        # 虚拟购买数
        self.wait_presence_element(AddGoods_BuyNumber).send_keys(shuliang1)
        # 售卖方式(一件代发)
        # self.wait_presence_element(AddGoods_BuyMode).click
        # 售卖方式（推广服务）
        # self.wait_presence_element(AddGoods_BuyMode2).click
        # 支持秒赔
        self.wait_presence_element(AddGoods_SecondCompensation).click()
        # 商品渠道
        # self.wait_presence_element(AddGoods_Retail).click()
        # 点击确定
        #self.wait_presence_element(AddGoods_ConfirmButton).click()

    def search_goods(self, OtherStyleTime,goodsname1):
        # 选择出商品名称搜索字段
        self.select_new(GoodsShelves_SwitchGoodsName, "商品名称")
        # 在搜索框输入搜索的名称
        self.wait_presence_element(GoodsShelves_SearchInput).send_keys(OtherStyleTime + goodsname1)
        # 点击搜索
        self.wait_presence_element(GoodsShelves_SearchButton).click()
        # 点击编辑
        self.wait_presence_element(GoodsShelves_EditButton).click()

    # 轮播图编辑
    def Edit_goods_lunbotu(self, pcituretudizhi,videodizhi):
        # 定位到轮播图
        self.wait_presence_element(EditGoods_TurnPicture).click()
        # 点击上传轮播图
        self.wait_presence_element(EditGoods_TurnPicture_UpPicture).click()
        # 点击选择文件并且上传进去
        self.wait_presence_element(EditGoods_TurnPicture_SelectFile).send_keys(pcituretudizhi)
        # 点击上传文件
        self.wait_presence_element(EditGoods_TurnPicture_UpFile).click()
        # 点击上传视频文件
        self.wait_presence_element(EditGoods_TurnPicture_UpVideo).click()
        # 点击选择视频文件并且上传进去
        self.wait_presence_element(EditGoods_TurnPicture_SelectFile).send_keys(videodizhi)
        # 点击上传文件
        self.wait_presence_element(EditGoods_TurnPicture_UpFile).click()
        # 点击确定
        self.wait_presence_element(EditGoods_TurnPicture_Submit).click()

    def edit_goods_description(self, descriptpicturedizhi):
        # 切换到描述信息Tab
        self.wait_presence_element(EditGoods_Description).click()
        # 上传产品图片
        self.wait_presence_element(EditGoods_Description_UpPicture).click()
        # 点击选择文件
        self.wait_presence_element(EditGoods_Description_SelectFile).send_keys(descriptpicturedizhi)
        # 点击确定上传
        self.wait_presence_element(EditGoods_Description_UpFile).click()

        """""""""""""""注意这里可能出错"""""""""""
        # 定位并选中文本框中的url
        value = self.wait_presence_element(EditGoods_Description_Text).get_attribute("value")
        # 输入描述（获取的url）
        self.wait_presence_element(EditGoods_Description_Describe).send_keys(value)
        # 点击确定
        self.wait_presence_element(EditGoods_Description_Submit)

    def edit_goods_cover(self, baozhiqi, baocunfangshi, inageaddress):
        # 切换的封面信息Tab
        self.wait_presence_element(EditGoods_Cover).click()
        # 选择二级品类
        self.wait_presence_element(EditGoods_Cover_SelectSecondType).click()
        # 选择运营分类(默认蔬菜)
        self.wait_presence_element(EditGoods_Cover_YunYingFenLei).click()
        # 选择商品辅助分类(默认自营)
        self.wait_presence_element(EditGoods_Cover_ShangPinFuZhuFenLei).click()
        # 选择拣货分类
        self.wait_presence_element(EditGoods_Cover_JianHuoFenLei).click()
        # 输入保质期
        self.wait_presence_element(EditGoods_Cover_BaoZhiQi).send_keys(baozhiqi)
        # 输入保存方式
        self.wait_presence_element(EditGoods_Cover_SaveWay).send_keys(baocunfangshi)
        """""""此位置需要调试"""""""
        # 团长信用售后时效(根据系统设置来定到底有没有)
        self.wait_presence_element(EditGoods_Cover_XinYongShiXiao)

        # 封面图片（选择图片）
        self.select_new(EditGoods_Cover_Cover, "图片")
        # 点击上传图片
        self.wait_presence_element(EditGoods_Cover_UpImage).click()
        # 点击选择文件并传进去
        self.wait_presence_element(EditGoods_Cover_SelectFile).send_keys(inageaddress)
        # 点击上传文件
        self.wait_presence_element(EditGoods_Cover_UpFile).click()
        """
        #上传视频
        self.new_sel(EditGoods_Cover_Cover,"视频")
        #点击上传视频
        self.wait_presence_element(EditGoods_Cover_UpVideo).click()
        #选择文件并上传
        self.wait_presence_element(EditGoods_Cover_SelectFile).send_keys(videoaddress)
        #点击上传文件
        self.wait_presence_element(EditGoods_Cover_UpFile).click()

        #上传视频封面
        self.wait_presence_element(EditGoods_Cover_VideoCover).click()
        #选择文件并上传
        self.wait_presence_element(EditGoods_Cover_SelectFile).send_keys(videopictureaddress)
        #点击上传文件
        self.wait_presence_element(EditGoods_Cover_UpFile).click()
        """

        # 封面价格
        self.wait_presence_element(EditGoods_Cover_CoverPrice)  # 此处需要调试
        # 团长专属
        # self.wait_presence_element()
        # 点击确定
        self.wait_presence_element(EditGoods_Cover_Submit).click()
    #创建规格(单规格)
    def edit_goods_guige(self,zhuguige,shuxingzhi):
        #切换到创建规格页面
        self.wait_presence_element(EditGoods_BuildGuiGe).click()
        #选择选择主规格
        self.select_new(EditGoods_BuildGuiGe_SelectG,zhuguige).click()
        #选择属性值
        self.select_new(EditGoods_BuildGuiGe_SelectA,shuxingzhi).click()
        #点击确定
        self.wait_presence_element(EditGoods_BuildGuiGe_Submit).click()
        #再次点击确定
        self.wait_presence_element(EditGoods_BuildGuiGe_SubmitAgain).click()
    def edit_goods_sku(self,guigetupian,guigemingcheng,price,bili):
        #切换到商品绑定sku页面
        self.wait_presence_element(EditGoods_ProductBindingSku).click
        #编辑
        self.wait_presence_element(EditGoods_SKU_ClickSearch).click()
        #点击请选择一项
        self.wait_presence_element(EditGoods_SKU_ClickSearch).clikc()
        #输入sku
        self.wait_presence_element(EditGoods_SKU_InputSkuId).send_keys(2520)
        #点击搜索出的sku
        self.wait_presence_element(EditGoods_SKU_ClickChooseSku).click()
        #点击添加
        self.wait_presence_element(EditGoods_SKU_ClickAddTo).click()
        #点击保存
        self.wait_presence_element(EditGoods_SKU_Button).click()
        #再次点击保存
        self.wait_presence_element(EditGoods_SKU_TwiceConfirm).click()
        #添加规格图片
        self.wait_presence_element(EditGoods_ProductBindingSku_GuiGePicture).click()
        #点击选择文件并选中
        self.wait_presence_element(EditGoods_ProductBindingSku_selectpicture).send_keys(guigetupian)
        #点击上传文件
        self.wait_presence_element(EditGoods_ProductBindingSku_UpFile).click()
        #购买显示规格
        self.wait_presence_element(EditGoods_ProductBindingSku_GuiGeName).send_keys(guigemingcheng)
        #主站团购价
        self.wait_presence_element(EditGoods_ProductBindingSku_MianStationPrice).send_keys(price)
        #佣金比例
        self.wait_presence_element(EditGoods_ProductBindingSku_CommissionRate).send_keys(bili)
        #点击起售
        self.wait_presence_element(EditGoods_ProductBindingSku_StartSell).click()
        #点击确定
        self.wait_presence_element(EditGoods_ProductBindingSku_Button).click()
        #再次点击确认
        self.wait_presence_element(EditGoods_ProductBindingSku_TwiceConfirm).click()

