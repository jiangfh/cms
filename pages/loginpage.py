# Author：yangyang
# Time：2019/10/24 10:36
# File: loginpage.py

import time    #为什么我这个是置灰的？

from baseclass.basepage import BasePage
from element.page_element import *

class LoginPage(BasePage):
    #def __init__(self,drvier):

    #from selenium import webdriver
    #driver=webdriver.Chrome()
    url='https://cms-test.nicetuan.net/admin/signin/'
    def login_success(self):
        #打开网址
        self.driver.get(self.url)
        #窗口最大化
        self.driver.maximize_window()
        #输入用户名
        self.wait_presence_element(username).send_keys('18331153752')
        #输入密码
        self.wait_presence_element(pwd).send_keys("wanghui")
        #点击登录
        self.wait_presence_element(button).click()
        #time.sleep(4)
    def login_error(self,username1,pwd1):
        #打开网址
        self.driver.get(self.url)
        #窗口最大化
        self.driver.maximize_window()
        #输入用户名
        self.wait_presence_element(username).send_keys(username1)
        #输入密码
        self.wait_presence_element(pwd).send_keys(pwd1)
        #点击登录
        self.wait_presence_element(button).click()
