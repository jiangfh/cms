from baseclass.basepage import BasePage
from element.page_element import *
class IndexPage(BasePage):
    def get_account_name(self):
        return self.wait_presence_element(Home_UserName)

    def enter_goods_on(self):
        #点击商品管理
        self.wait_presence_element(Home_GoodsManagement).click()
        #点击商品上架
        self.wait_presence_element(Home_GoodsShelves).click()