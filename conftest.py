#import os

import pytest
import allure
from selenium import webdriver
from pages.loginpage import LoginPage

@pytest.fixture()
def init_web():
    chrome_options=webdriver.ChromeOptions()
    chrome_options.add_argument("--window-size=1366,768")
    chrome_options.add_argument("--start-maximized")
    chrome_options.add_argument('--disable-infobars')
    driver=webdriver.Chrome(executable_path="D:\Programs\Python\Python38\chromedriver.exe",options=chrome_options)
    login_page=LoginPage(driver)
    login_page.login_success()
    yield driver,login_page
    driver.quit()