import random
from selenium.webdriver.common.by import By
"""页面元素"""
"""登录"""
url='https://cms-test.nicetuan.net/admin/signin/'
#账户
username=(By.XPATH,('//input[@class="mPone"]'))
#密码
pwd=(By.XPATH,('//input[@id="captcha"]'))
#点击登录
button=(By.XPATH,('//button[@class="submit"]'))


"""首页元素"""
#断言时定位的用户名
Home_UserName=(By.XPATH,('//span[@class="username username-hide-on-mobile"]'))

"""想要去编辑或者新建商品，首先要点击商品管理，点击商品上架，进入商品上架详情页"""
#商品管理
Home_GoodsManagement=(By.XPATH,('//a[@id="navi318"]'))
#商品上架
#Home_GoodsShelves=(By.XPATH,('//a[@id="navi309"]'))
Home_GoodsShelves=(By.XPATH,("//a[@id='navi309']"))

"""""""""""""""""""""""""""""""""""""新建商品"""""""""""""""""""""""""""""""""""""""""""""""""
#如果想要新建商品，要点击添加商品按钮
AddGoods=(By.XPATH,'//a[@class="btn btn-primary"]')
#点击寻味师选择框
AddGoods_Xunwei=(By.XPATH,('//button[@type="button"]'))
#选择寻味师
AddGoods_XunWeiFirst=(By.XPATH,('//a/span[contains(text(),"河北省寻味师1")]'))
#输入商品名称
AddGoods_GoodsName=(By.XPATH,('//input[@name="title"]'))
#输入商品简称
AddGoods_GoodsShortName=(By.XPATH,('//input[@name="abbreviation"]'))
#所属站点
AddGoods_ZhanDian=(By.XPATH,('//select[@name="city_id"]'))
#卖点介绍
AddGoods_SellPoint=(By.XPATH,('//input[@name="content"]'))
#产地
AddGoods_GoodArea=(By.XPATH,('//input[@name="growarea"]'))
#虚拟购买数
AddGoods_BuyNumber=(By.XPATH,('//input[@name="manualsetbuyertotal"]'))
#售卖方式（一件代发）
AddGoods_BuyMode=(By.XPATH,('//input[@id="sale_method" and @value="1"]'))
#售卖方式(推广服务)
AddGoods_BuyMode2=(By.XPATH,('//input[@id="iyunbao_method" and @vale="3"]'))
#支持秒赔
AddGoods_SecondCompensation=(By.XPATH,('//input[@type="radio"and @name="is_credit"and @value="1"]'))
#零售通
AddGoods_Retail=(By.XPATH,('//input[@id="source"]'))
#点击确认
AddGoods_ConfirmButton=(By.XPATH,('//button[@type="submit"]'))
#以上是新建商品的基本信息
#在建立商品后，由于逻辑规则是当点击确认后页面会跳转至商品管理页面，
# 所以当需要在对该新建商品继续编辑时，需要找到该商品编辑进入后才能对其进行后续编辑
#此过程暂时没有好的办法过度，所以暂时忽略
#临时过渡
#切换到按商品名称搜索
GoodsShelves_SwitchGoodsName=(By.XPATH,('//select[@name="searchfield"]'))
#搜索输入框
GoodsShelves_SearchInput=(By.XPATH,('//input[@name="search_key"]'))
#搜索按钮
GoodsShelves_SearchButton=(By.XPATH,('//button[@title="搜索"]'))
#编辑按钮
GoodsShelves_EditButton=(By.XPATH,('//a[contains(text(),"编辑")]'))

"""""""""""""""""""""""""""""""""""""轮播图"""""""""""""""""""""""""""""""""""""""""""""""""
#定位到轮播图(此定位方法与大法有差异，后期注意)
EditGoods_TurnPicture=(By.XPATH,('//a[contain(text(),"轮播图")]'))
#点击上传轮播图
EditGoods_TurnPicture_UpPicture=(By.XPATH,('//*[@id="upload_merch_image10"]'))
#点击选择文件
EditGoods_TurnPicture_SelectFile=(By.XPATH,('//*[@id="file2upload"]'))
#点击上传文件
EditGoods_TurnPicture_UpFile=(By.XPATH,('//*[@id="btnfileupload"]'))


#点击上传视频文件
EditGoods_TurnPicture_UpVideo=(By.XPATH,('//*[@id="newbie_teaching_video10"]'))
#点击选择文件
EditGoods_TurnPicture_SelectFile=(By.XPATH,('//*[@id="file2upload"]'))
#点击上传文件
EditGoods_TurnPicture_UpFile=(By.XPATH,('//*[@id="btnfileupload"]'))
#点击确定
EditGoods_TurnPicture_Submit=(By.XPATH,('//button[@type="submit"]'))

"""""""""""""""""""""""""""""""""""""描述信息"""""""""""""""""""""""""""""""""""""""""""""""""
#切换到描述信息Tab
EditGoods_Description=(By.XPATH,('//a[contains(text(),"描述信息")]'))
#点击上传产品图片
EditGoods_Description_UpPicture=(By.XPATH,('//*[@id="upload_merch_image"]'))
#点击选择文件
EditGoods_Description_SelectFile=(By.XPATH,('//*[@id="file2upload"]'))
#点击上传文件
EditGoods_Description_UpFile=(By.XPATH,('//*[@id="btnfileupload"]'))
#定位文本框
EditGoods_Description_Text=(By.XPATH,('//input[@name="upyunfiles[image][]"]'))
#描述
EditGoods_Description_Describe=(By.XPATH,('//*[@id="description"]'))
#点击确定
EditGoods_Description_Submit=(By.XPATH,('//button[@type="submit"]'))

"""""""""""""""""""""""""""""""""""""封面信息"""""""""""""""""""""""""""""""""""""""""""""""""
#切换到封面信息
EditGoods_Cover=(By.XPATH,('//a[contains(text(),"封面信息")]'))
#选择二级品类
#后台二级分类采用随机选择
suiji=random.choice((687,688,689,690,691,693,786,810,796,811,790,797,812,798,813,799,814,800))
EditGoods_Cover_SelectSecondType=(By.XPATH,('//input[@value={}]'.format(suiji)))
#运营分类选择(蔬菜)
EditGoods_Cover_YunYingFenLei=(By.XPATH,('//input[@vale="143"]'))
#商品辅助分类（自营）
EditGoods_Cover_ShangPinFuZhuFenLei=(By.XPATH,('//input[@value="614"]'))
#拣货分类（蔬菜）
EditGoods_Cover_JianHuoFenLei=(By.XPATH,('//input[@value="11"]'))
#保质期
EditGoods_Cover_BaoZhiQi=(By.XPATH,('//input[@name="expiration_date"]'))
#保存方式
EditGoods_Cover_SaveWay=(By.XPATH,('//input[@name="saveway"]'))
#团长信用售后时效
#该时效用随机来确定
suiji2=random.choice((1,2,3,4,5))
#注意这个元素是否正确
EditGoods_Cover_XinYongShiXiao=(By.XPATH,('input[@id="credit_limit_time"and@vale={}]'.format(suiji2)))

#封面图片的选择（分两种）
#后续的点击选择文件与点击上传文件在该页面共用相同元素在下面展示
EditGoods_Cover_Cover=(By.XPATH,('//select[@name="cover_type"]'))
##############################################
#1、选择图片
EditGoods_Cover_Image=(By.XPATH,('//option[@vale="image"]'))
#点击上传图片
EditGoods_Cover_UpImage=(By.XPATH,('//a[@id="upload_cover_image"]'))
####################################################################
#2、选择视频
EditGoods_Cover_Video=(By.XPATH,('//option[@vale="video"]'))
#点击上传视频
EditGoods_Cover_UpVideo=(By.XPATH,('//a[@id="upload_cover_video"]'))

#点击上传视频图片（视频封面）
EditGoods_Cover_VideoCover=(By.XPATH,('//a[@id="upload_cover_video_image"]'))

############################上传视频、图片需要的公共元素##################################
#点击选择文件
EditGoods_Cover_SelectFile=(By.XPATH,('//input[@id=file2upload]'))
#点击上传文件
EditGoods_Cover_UpFile=(By.XPATH,('//button[@id="btnfileupload"]'))
############################上传视频、图片需要的公共元素##################################

#封面价格(随机放几个值)
suiji3=random.choice((10,11,12,13))
EditGoods_Cover_CoverPrice=(By.XPATH,('//input[@id="origin_price"and @vale={}]'.format(suiji3)))
#团长专属(待定吧)
EditGoods_Cover_TuanZhangZhuanShu=(By.XPATH,)
#点击确定
EditGoods_Cover_Submit=(By.XPATH,('//button[@type="submit"]'))

"""""""""""""""""""""""""""""""""""""创建规格"""""""""""""""""""""""""""""""""""""""""""""""""
EditGoods_BuildGuiGe=(By.XPATH,('//a[contains(text(),"创建规格")]'))
#添加规格
EditGoods_BuildGuiGe_AddGuiGe=(By.XPATH,('//*[@class="btn btn-primary add-attr"]/i'))
#添加属性值
EditGoods_BuildGuiGe_AddAttribute=(By.XPATH,('//*[@id="add_master_spec"]/i'))
#选择规格
EditGoods_BuildGuiGe_SelectG=(By.XPATH,('//select[@name="master_attribute[]"]'))
#选择主规格
EditGoods_BuildGuiGe_SelectMainGuiGe=(By.XPATH,('//option[@value="2"]'))
#选择属性
EditGoods_BuildGuiGe_SelectA=(By.XPATH,('//select[@name="master_spec[]"]'))
#选择属性值
EditGoods_BuildGuiGe_SelectAttribute=(By.XPATH,('//option[@vale="3"]'))
#点击确定
EditGoods_BuildGuiGe_Submit=(By.XPATH,('//*[@id="saveAttribute"]'))
#再次点击确定
EditGoods_BuildGuiGe_SubmitAgain=(By.XPATH,('//div[@class="btn sure"]'))

"""""""""""""""""""""""""""""""""""""商品绑定sku"""""""""""""""""""""""""""""""""""""""""""""""""

#规格图片
EditGoods_ProductBindingSku_GuiGePicture=(By.XPATH,('//a[contains(text(),"点击上传")]'))
#选中并上传图片
EditGoods_ProductBindingSku_selectpicture=(By.XPATH,('//input[@id=file2upload]'))
#上传文件
EditGoods_ProductBindingSku_UpFile=(By.XPATH,('//button[@id="btnfileupload"]'))
#购买显示规格名称
EditGoods_ProductBindingSku_GuiGeName=(By.XPATH,('//input[@name="merchtype_name[]"]'))
#主站团购价
EditGoods_ProductBindingSku_MianStationPrice=(By.XPATH,('//input[@name="price[]"]'))
#佣金比例
EditGoods_ProductBindingSku_CommissionRate=(By.XPATH,('//input[@name="brokerage[]"]'))
################################################################################################
#预售类型(该选择默认是非预售的)
EditGoods_ProductBindingSku_Select=(By.XPATH,('//select[@name="preselltype[]"]'))#点击选择框
#以后用到在写吧
# EditGoods_ProductBindingSku_SelectType=(By.XPATH,())
##################################################################################################
#点击确定按钮
EditGoods_ProductBindingSku_Button=(By.XPATH,('//button[@type="button"]'))
#点击二次确认按钮
EditGoods_ProductBindingSku_TwiceConfirm = (By.XPATH,"//div[@class='btn sure']")
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
#点击编辑
EditGoods_ProductBindingSku_Edit=(By.XPATH,('//a[contains(text(),"编辑")]'))
#点击起售
EditGoods_ProductBindingSku_StartSell=(By.XPATH,('//a[@class="btn btn-xs btn-success sell-btn"]'))
"""""""""""""""""""""""""""""""""""""""""编辑sku"""""""""""""""""""""""""""""""""""""""""
#添加sku页面
EditGoods_ProductBindingSku=(By.XPATH,('//a[contains(text(),"商品绑定sku")]'))
#点击请选择一项
EditGoods_SKU_ClickSearch=(By.XPATH,('//button[@data-id="js-skuid-select"]'))
#输入搜索的skui2520
EditGoods_SKU_InputSkuId=(By.XPATH,('//input[@class="form-control"]'))
#点击搜索出的sku
EditGoods_SKU_ClickChooseSku=(By.XPATH,('//span[contains(text,"2520")]'))
#点击添加
EditGoods_SKU_ClickAddTo=(By.XPATH,('//a[class="btn btn-primary addSkuBtn"]'))
#点击保存
EditGoods_SKU_Button=(By.XPATH,('//button[@id="saveBtn"]'))
#点击再次保存
EditGoods_SKU_TwiceConfirm=(By.XPATH,('//div[@class="btn sure"]'))